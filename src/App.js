import React, { Component } from 'react';
import { connect } from 'react-redux';

import './app.css';

import {
    getAllTodos,
    onChangeTodoStatus,
    createTodo,
    updateTodo,
    onClickDeleteCompleted,
    updateFormEntry,
    initializeProps,
    onClickChangeTab
} from './store/actions/todoActions';

class App extends Component {
    constructor() {
        super();
        this.submitHandler = this.submitHandler.bind(this);
        this.onClickDleteHandler = this.onClickDleteHandler.bind(this);
    }

    componentDidMount() {
        // mainly used for api call
        const { getAllTodos } = this.props;
        getAllTodos();
    }

    submitHandler(e) {
        e.preventDefault();
        const { id, message, tabStatus, createTodo, updateTodo, initializeProps, onClickChangeTab } = this.props;
        const todo = { id: undefined, message: ''}
        if (message !== '' && id === undefined) {
            createTodo(message);
        } else if (message !== '' && id !== undefined) {
            updateTodo({ id, message });
        }
        initializeProps(todo);
        onClickChangeTab(tabStatus);
    }

    onClickDleteHandler() {
        const { tabStatus,onClickChangeTab, onClickDeleteCompleted } = this.props;
        onClickDeleteCompleted();
        onClickChangeTab(tabStatus);
    }

    render() {
        const {
            todoList,
            message,
            todoLeft,
            todoDone,
            tabStatus,
            onChangeTodoStatus,
            updateFormEntry,
            onClickChangeTab,
            initializeProps } = this.props;
        return (
            <div className="container-sm">
                <h3 className="text-center">ToDo</h3>
                <div className="card border-0 rounded-0 shadow p-4">
                    <form onSubmit={this.submitHandler}>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control border-0 rounded-0"
                                placeholder="Whats needs to be done?"
                                value={message}
                                name="message"
                                onChange={(event) => { updateFormEntry(event); }}
                            />
                        </div>
                    
                    { todoList && todoList.map(function(todo, index) {
                        return (
                            <div key={index} className="form-group">
                                <div className="form-check">
                                    <input 
                                        className="form-check-input"
                                        type="checkbox"
                                        onChange={() => { onChangeTodoStatus(todo.id, todo.status); }}
                                        value={todo.status}
                                        checked={todo.status}
                                    />
                                    <label
                                        className="form-check-label"
                                        htmlFor="gridCheck"
                                        style={{ cursor: 'pointer' }}
                                        onClick={() => initializeProps(todo)}
                                    >
                                        {todo.status ? <del>{todo.message}</del> : `${todo.message}`}
                                    </label>
                                </div>
                            </div>
                            )
                        })
                    }
                    </form>
                    
                    { (todoDone || todoLeft) > 0 && <div className="text-muted row">
                        <div className="col-md-4 text-center">{ todoLeft } items left</div>
                        <div className="col-md-4 text-center">
                            <div className="btn-group btn-group-sm" role="group" aria-label="tab option">
                                <button
                                    type="button"
                                    className={tabStatus === 'All' ? 'btn btn-light active' : 'btn btn-light'}
                                    onClick={() => onClickChangeTab('All')}
                                >
                                    All
                                </button>
                                <button
                                    type="button"
                                    className={tabStatus === 'Active' ? 'btn btn-light active' : 'btn btn-light'}
                                    onClick={() => onClickChangeTab('Active')}
                                >
                                    Active
                                </button>
                                <button
                                    type="button"
                                    className={tabStatus === 'Completed' ? 'btn btn-light active' : 'btn btn-light'}
                                    onClick={() => onClickChangeTab('Completed')}
                                >
                                    Completed
                                </button>
                            </div>
                        </div>
                        
                        { todoDone > 0 && <div
                            className="col-md-4 text-center"
                            style={{ cursor: 'pointer' }}
                            onClick={() => this.onClickDleteHandler()}
                        >
                            Clear completed
                        </div>}
                    </div>}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { todo } = state;
    return {
        todoList: todo.todoList,
        // for form handling
        message: todo.message,
        // for todo left status
        todoLeft: todo.todoLeft,
        todoDone: todo.todoDone,
        tabStatus: todo.tabStatus,
        // for updating a todo
        id: todo.id,
    }; 
}
  
function mapDispatchToProps(dispatch) {
    return {
        getAllTodos: () => { dispatch(getAllTodos()); },
        createTodo: (data) => { dispatch(createTodo(data)); },
        updateTodo: (data) => { dispatch(updateTodo(data)); },
        onChangeTodoStatus: (id, status) => { dispatch(onChangeTodoStatus(id, status)); },
        onClickChangeTab: (event) => { dispatch(onClickChangeTab(event)); },
        onClickDeleteCompleted: () => { dispatch(onClickDeleteCompleted()); },
        updateFormEntry: (event) => { dispatch(updateFormEntry(event)); },
        initializeProps: (data) => { dispatch(initializeProps(data)); },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
