import {
    GET_ALL_TODOS,
    CREATE_TODO,
    CHANGE_TODO_STATUS,
    DELETE_TODOS,
    UPDATE_FORM_ENTRY,
    INITIALIZE_FORM_ENTRY,
    CHANGE_TAB,
    UPDATE_TODO_MESSAGE
} from '../actions/types';

const initialState = {
    message: ''
};

export default (state = initialState, action) => {

    switch (action.type) {
        case GET_ALL_TODOS: {
            const stored = JSON.parse(localStorage.getItem('todoData'));
            let todoListLeft = 0;
            let todoListDone = 0;

            if (!stored) {
                localStorage.setItem("todoData", JSON.stringify([]));
            } else if (stored) {
                stored.forEach(function (todo) {
                    if (todo.status === false) {
                        todoListLeft += 1;
                    } else {
                        todoListDone += 1;
                    }
                });
            }
            return {
                ...state,
                todoList: stored,
                todoLeft: todoListLeft,
                todoDone: todoListDone,
                tabStatus: 'All'
            };
        }

        case CREATE_TODO: {
            const { payload } = action;
            const stored = JSON.parse(localStorage.getItem('todoData'));
            let newTodo = {};

            if (!stored || stored.length === 0) {
                newTodo = { id: 1, message: payload, status: false }
            } else if(stored.length > 0) {
                const newId = stored[stored.length - 1].id + 1;
                newTodo = { id: newId, message: payload, status: false };
            }

            stored.push(newTodo);
            localStorage.setItem("todoData", JSON.stringify(stored));

            return {
                ...state,
                todoList: stored,
                todoLeft: state.todoLeft + 1,
                tabStatus: 'All'
            };
        }

        case UPDATE_TODO_MESSAGE: {
            const { id, message } = action.payload;
            const stored = JSON.parse(localStorage.getItem('todoData'));
            const idx = state.todoList.findIndex((todo) => todo.id === id);

            if (!stored || stored.length === 0 || idx < 0) {
                return state;
            }

            const todoList = [
                ...stored.slice(0, idx),
                {
                    ...stored[idx],
                    id,
                    message
                },
                ...stored.slice(idx + 1)
            ];

            localStorage.setItem("todoData", JSON.stringify(todoList));

            return {
                ...state,
                todoList
            };
        }

        case CHANGE_TODO_STATUS: {
            const { id, status } = action.payload;
            const idx = state.todoList.findIndex((todo) => todo.id === id);
            const stored = JSON.parse(localStorage.getItem('todoData'));

            if (idx < 0) {
                return state;
            }

            const todoList = [
                ...stored.slice(0, idx),
                {
                    id: idx,
                    message: stored[idx].message,
                    status: status ? false : true
                },
                ...stored.slice(idx + 1)
            ];

            localStorage.setItem("todoData", JSON.stringify(todoList));

            let todoListLeft = 0;
            let todoListDone = 0;
            todoList.forEach(function (todo) {
                if (todo.status === false) {
                    todoListLeft += 1;
                } else {
                    todoListDone += 1;
                }
            });

            return {
                ...state,
                todoList,
                todoLeft: todoListLeft,
                todoDone: todoListDone
            };
        }

        case DELETE_TODOS: {
            const stored = JSON.parse(localStorage.getItem('todoData'));

            if (!stored || stored.length === 0) {
                return state;
            }

            let newTodoList = [];

            stored.forEach(function (todo) {
                if (todo.status === false) {
                    newTodoList.push(todo);
                }
            });

            localStorage.setItem("todoData", JSON.stringify(newTodoList));

            return {
                ...state,
                todoList: newTodoList,
                todoLeft: newTodoList.length,
                todoDone: 0,
                tabStatus: 'All'
            };
        }

        case UPDATE_FORM_ENTRY: {
            const event = action.payload;
            return {
                ...state,
                [event.target.name]: event.target.value
            };
        }

        case INITIALIZE_FORM_ENTRY: {
            const { id, message } = action.payload;
            return {
                ...state,
                message,
                id
            };
        }

        case CHANGE_TAB: {
            const stored = JSON.parse(localStorage.getItem('todoData'));
            const { payload } = action;

            if (payload === 'Completed') {
                let newTodoList = [];
                stored.forEach(function (todo) {
                    if (todo.status === true) {
                        newTodoList.push(todo);
                    }
                });
                return {
                    ...state,
                    todoList: newTodoList,
                    tabStatus: 'Completed'
                }
            } else if (payload === 'Active') {
                let newTodoList = [];
                stored.forEach(function (todo) {
                    if (todo.status === false) {
                        newTodoList.push(todo);
                    }
                });
                return {
                    ...state,
                    todoList: newTodoList,
                    tabStatus: 'Active'
                }
            }
            return {
                ...state,
                todoList: stored,
                tabStatus: 'All'
            }
        }

        default:
            return state;
    }
}; 
