import { combineReducers } from 'redux';
import TodoReducer from './reducerTodo';

const rootReducer = combineReducers({
  todo: TodoReducer
});

export default rootReducer;
