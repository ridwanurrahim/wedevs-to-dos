import {
    GET_ALL_TODOS,
    CREATE_TODO,
    CHANGE_TODO_STATUS,
    DELETE_TODOS,
    UPDATE_FORM_ENTRY,
    INITIALIZE_FORM_ENTRY,
    CHANGE_TAB,
    UPDATE_TODO_MESSAGE
} from './types';

export function getAllTodos() {
    return (dispatch) => {
        dispatch({
            type: GET_ALL_TODOS
        });
    };
}

export function onClickDeleteCompleted() {
    return (dispatch) => {
        dispatch({
            type: DELETE_TODOS
        });
    };
}

export function onChangeTodoStatus(id, status) {
    return (dispatch) => {
        dispatch({
            type: CHANGE_TODO_STATUS,
            payload: { id, status }
        });
    };
}

export function createTodo(data) {
    return (dispatch) => {
        dispatch({
            type: CREATE_TODO,
            payload: data
        });
    };
}

export function updateTodo(data) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_TODO_MESSAGE,
            payload: data
        });
    };
}

export function initializeProps(data) {
    return (dispatch) => {
        dispatch({
            type: INITIALIZE_FORM_ENTRY,
            payload: data
        });
    };
}

export function updateFormEntry(event) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_FORM_ENTRY,
            payload: event
        });
    };
}

export function onClickChangeTab(event) {
    return (dispatch) => {
        dispatch({
            type: CHANGE_TAB,
            payload: event
        });
    };
}